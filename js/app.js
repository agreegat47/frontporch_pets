jQuery(function() {
	initFoundation();
	slickSlider();
});

function initFoundation() {
	jQuery(document).foundation();
}



function slickSlider(){
    jQuery('.big-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        fade: true,
        arrows: true,
        cssEase: 'ease',
        autoplay: true,
        adaptiveHeight: false,
        autoplaySpeed: 3000,
        variableWidth: false
    });
}
